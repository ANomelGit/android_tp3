package ci.esatic.multimedia;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Message extends AppCompatActivity {

    EditText txt_numberPhone, txt_message;
    Button buttonRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        txt_numberPhone = (EditText) findViewById(R.id.txt_numberPhone);
        txt_message = (EditText) findViewById(R.id.txt_message);
        buttonRetour = (Button) findViewById(R.id.btnRetour_msg);

        // Retour
        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Message.this, MenuActivity.class);
                startActivity(intent);
            }
        });
    }

    public void envoyer_message(View view) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED){
            MyMessage();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 0);
        }
    }

    private void MyMessage() {
        String phoneNumber = txt_numberPhone.getText().toString().trim();
        String Message = txt_message.getText().toString().trim();

        if (!txt_numberPhone.getText().toString().equals("") || !txt_message.getText().toString().equals("")){
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, Message, null, null);
            Toast.makeText(this, "Message envoyé !", Toast.LENGTH_SHORT).show();
            txt_message.setText("");
        } else {
            Toast.makeText(this, "Veuillez entrer un Numéro ou un Message !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 0:
                if (grantResults.length >= 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    MyMessage();
                } else {
                    Toast.makeText(this, "Vous n'avez pas de permissions pour envoyer un message  !", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}