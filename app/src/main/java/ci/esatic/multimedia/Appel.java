package ci.esatic.multimedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class Appel extends AppCompatActivity {

    EditText phoneNo;
    ImageButton callBtn;
    Button buttonRetour;
    static  int PERMISSION_CODE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appel);

        phoneNo = findViewById(R.id.txt_phone_number_appel);
        callBtn = findViewById(R.id.btn_appel);
        buttonRetour = findViewById(R.id.btnRetour_appel);

        if (ContextCompat.checkSelfPermission(Appel.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(Appel.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CODE);
        }

        callBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                String phoneno = phoneNo.getText().toString().trim();
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:" + phoneno));
                startActivity(i);
            }
        });

        // Retour
        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Appel.this, MenuActivity.class);
                startActivity(intent);
            }
        });
    }
}