package ci.esatic.multimedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MenuActivity extends AppCompatActivity {

    private ImageButton imageButtonCall;
    private ImageButton imageButtonSms;
    private ImageButton imageButtonMusic;
    private ImageButton imageButtonVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        imageButtonCall = (ImageButton) findViewById(R.id.imgBtn_call);
        imageButtonSms = (ImageButton) findViewById(R.id.imgBtn_sms);
        imageButtonMusic = (ImageButton) findViewById(R.id.imgBtn_music);
        imageButtonVideo = (ImageButton) findViewById(R.id.imgBtn_video);

        imageButtonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Appel.class);
                startActivity(intent);
            }
        });

        imageButtonSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Message.class);
                startActivity(intent);
            }
        });

        imageButtonMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AudioActivity.class);
                startActivity(intent);
            }
        });

        imageButtonVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, VideoActivity.class);
                startActivity(intent);
            }
        });
    }
}