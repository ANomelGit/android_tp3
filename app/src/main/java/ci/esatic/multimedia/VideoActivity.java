package ci.esatic.multimedia;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.VideoView;

import java.util.ArrayList;

public class VideoActivity extends AppCompatActivity {

    Button btn_chooseFile;
    Intent myFileIntent;
    Button buttonRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        buttonRetour = findViewById(R.id.btnRetour_video);

        String videoPath = "android.resource://" + getPackageName() + "/" + R.raw.video_1;

        VideoView videoView = findViewById(R.id.video_view);
        Uri uri = Uri.parse(videoPath);
        videoView.setVideoURI(uri);
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController((mediaController));
        mediaController.setAnchorView(videoView);

        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

//        btn_chooseFile = (Button) findViewById(R.id.choose_filebtn);
//
//        btn_chooseFile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                myFileIntent.setType("*/*");
//                startActivityForResult(myFileIntent, 10);
//            }
//        });
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (requestCode){
//            case 10:
//
//                if (resultCode == RESULT_OK){
//                    String videoPath = data.getData().getPath();
//                    setVideoPAth(videoPath);
//                }
//                break;
//        }
//    }
}